﻿-- =============================================
-- Script Template
-- =============================================
DBCC CHECKIDENT('dbo.Chapter', RESEED, 1);

BEGIN TRAN
	DELETE FROM dbo.Chapter;
	INSERT INTO dbo.Chapter (ChapterName, Address, ContectNumber, TwitterWidget, MapLocation, Active) VALUES (
		'Seattle',
		'Ahmadiyya Muslim Community 19212 Highway 99<br /> Lynnwood, WA,98036<br />phone:(425)954-7526<br />',
		'(425)954-7526',
		' <a class="twitter-timeline" href="https://twitter.com/amiseattle" data-widget-id="402179807552606208">Tweets by amiseattle</a> <script>    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? ''http'' : ''https''; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>',
		'<iframe src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x549005ea37519b35%3A0xac6b78d0e820b490!2s100+172nd+Pl+SE%2C+Bothell%2C+WA+98012!5e0!3m2!1sen!2sus!4v1385517040099" width="300" height="450" frameborder="0" style="border:0"></iframe>',
		1);

	IF (@@ERROR <> 0)
		ROLLBACK TRAN
	ELSE
	COMMIT TRAN
