﻿CREATE TABLE [dbo].[Chapter] (
    [ChapterID]     INT            IDENTITY (1, 1) NOT NULL,
    [ChapterName]   NVARCHAR (100) NOT NULL,
    [Address]       NVARCHAR (500) NULL,
    [ContectNumber] NVARCHAR (50)  NULL,
    [TwitterWidget] NVARCHAR (500) NULL,
    [MapLocation]   NVARCHAR (500) NULL,
    [Active]        BIT            NULL
);

