﻿CREATE TABLE [dbo].[EventRegistration] (
    [ID]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (100) NOT NULL,
    [Email]           NVARCHAR (100) NOT NULL,
    [Phone]           NVARCHAR (50)  NULL,
    [Message]         NVARCHAR (MAX) NOT NULL,
    [ChapterID]       INT            NOT NULL,
    [EventID]         INT            NOT NULL,
    [NumberOfPersons] INT            NULL
);

