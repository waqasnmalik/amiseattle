﻿CREATE TABLE [dbo].[Events] (
    [EventID]     INT            IDENTITY (1, 1) NOT NULL,
    [ChapterID]   INT            NULL,
    [Title]       NVARCHAR (MAX) NOT NULL,
    [Location]    NVARCHAR (MAX) NULL,
    [Details]     NVARCHAR (MAX) NULL,
    [Date]        NVARCHAR (50)  NULL,
    [StartTime]   NVARCHAR (50)  NULL,
    [EndTime]     NVARCHAR (50)  NULL,
    [Active]      BIT            NULL,
    [Image]       NVARCHAR (255) NULL,
    [DetailFile]  NVARCHAR (255) NULL,
    [MtaLink]     NVARCHAR (255) NULL,
    [AlislamLink] NVARCHAR (255) NULL,
    [YoutubeLink] NVARCHAR (255) NULL,
    [MapLocation] NVARCHAR (555) NULL
);

