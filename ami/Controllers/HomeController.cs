﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ami.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            ViewBag.DayOfWeek = System.DateTime.Now.DayOfWeek;
            ViewBag.SelectedValue = "My Value";
            return View();
        }
        public ActionResult Contact()
        {
            var vv = ViewBag.SelectedValue;
            return View();
        }
        public ActionResult About(int id)
        {
            return View();
        }

        public ActionResult EventRegistration()
        {
            return View();
        }
    }
}
