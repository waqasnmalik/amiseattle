﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ami.Models
{
    public class AmiModel
    {
        private int id = 0;
        private void setid(int i)
        {
            id = i;
        }
        public int getID()
        {
            return 5;
        }

        public IEnumerable<AmiChapter> GetChapter()
        {
            setid(5);
            
            List<AmiChapter> amiChapterList = new List<AmiChapter>();
             IEnumerable<AmiChapter> data =amiChapterList as IEnumerable<AmiChapter>;
             AmiChapter amic1 = new AmiChapter();
            amic1.ChapterID=1;
            amic1.ChapterName ="Seattle";
            amiChapterList.Add(amic1);
            AmiChapter amic2 = new AmiChapter();
            amic2.ChapterID = 2;
            amic2.ChapterName = "Portland";
             amiChapterList.Add(amic2);
             data = amiChapterList as IEnumerable<AmiChapter>;
            /*
            try
            {
                DateTime date = new DateTime();
                string queryString =
           "SELECT ChapterID, ChapterName FROM AmiChapter";
                SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ami_local_Connection"].ConnectionString);
                connection.Open();
                SqlCommand command =
                    new SqlCommand(queryString, connection);
                SqlDataReader reader = command.ExecuteReader();
                amiChapterList.Clear();
                try
                {
                    while (reader.Read())
                    {
                        AmiChapter amiChapter = new AmiChapter();
                        amiChapter.ChapterID = short.Parse(reader["ChapterID"].ToString());
                        amiChapter.ChapterName = reader["ChapterName"].ToString();
                        amiChapterList.Add(amiChapter);
                    }
                    data = amiChapterList as IEnumerable<AmiChapter>;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                }
                finally
                {
                    connection.Close();
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                return data;
            }*/
            return data;
        }
    }

    public class AmiChapter
    {
        public short ChapterID { get; set; }
        public string ChapterName { get; set; }
    }

    public class Contact
    {
        [Required]
        [DisplayName("Name *")]
        public string Name { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DisplayName("Email * (ex@gmail.com)")]
        public string Email { get; set; }
        [DisplayName("Phone(123-456-7891)")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid. format is (123-456-7891).")]
        public byte[] PhoneNo { get; set; }
        [Required(ErrorMessage = "Message can not be left blank")]
        [DisplayName("Message *")]    
        public string Message { get; set; }
    }

    public class EventRegistration
    {
        [Required]
        [DisplayName("Name *")]
        public string Name { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DisplayName("Email * (ex@gmail.com)")]
        public string Email { get; set; }
        [DisplayName("Phone (123-456-7891)")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid. format is (123-456-7891).")]
        public byte[] PhoneNo { get; set; }
        [DisplayName("Number of persons comming with you")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Only numbers are allowed in this field")]
        public byte[] NumberOfPersons { get; set; }
        [DisplayName("Message")]
        public string Message { get; set; }
    }
}