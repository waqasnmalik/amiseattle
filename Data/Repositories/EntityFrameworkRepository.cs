﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ami.Data.Entities;
using System.Data.Entity;

namespace ami.Data.Repositories
{
    /// <summary>
    /// The entity framework repository.
    /// </summary>
    /// <typeparam name="TEntity">The entity's type.
    /// </typeparam>
    public class EntityFrameworkRepository<TEntity> : IDisposable, IRepository<TEntity>
        where TEntity : class, IEntity, new()
    {
        /// <summary>
        /// The context.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFrameworkRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="ArgumentNullException">The DbContext is null.
        /// </exception>
        public EntityFrameworkRepository(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context", "Value cannot be null.");
            }

            this.context = context;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TEntity> Get()
        {
            return this.context.Set<TEntity>();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TEntity> Get(int id)
        {
            return this.context.Set<TEntity>().Where(e => e.Id == id);
        }

        /// <summary>
        /// The get single.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity GetSingle(int id)
        {
            return this.context.Set<TEntity>().Single(e => e.Id == id);
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Add(TEntity entity)
        {
            var entry = this.context.Entry(entity);
            entry.State = EntityState.Added;
            this.context.Set<TEntity>().Add(entity);
            this.Save();
            return entity.Id;
        }

        /// <summary>
        /// The change.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Change(TEntity entity)
        {
            var entry = this.context.Entry(entity);
            entry.State = EntityState.Modified;
            this.Save();
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public void Remove(TEntity entity)
        {
            var entry = this.context.Entry(entity);
            entry.State = EntityState.Deleted;
            this.Save();
        }

        /// <summary>
        ///     IDisposable implementation
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// this method can be called in two cases, either explicitly
        ///     If disposing equals true, the method has been called directly
        ///     or indirectly by a user's code. Managed and unmanaged resources
        ///     can be disposed.
        ///     If disposing equals false, the method has been called by the
        ///     runtime from inside the finalizer and you should not reference
        ///     other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="disposing">
        /// True if the Dispose method has already been called, else false.
        /// </param>
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this.context != null)
                    {
                        this.context.Dispose();
                        this.context = null;
                    }
                }

                // Note disposing has been done.
                this.disposed = true;
            }
        }

        /// <summary>
        /// The save.
        /// </summary>
        private void Save()
        {
            this.context.SaveChanges();
        }
    }
}