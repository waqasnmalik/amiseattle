﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ami.Data.Entities;

namespace ami.Data.Repositories
{
    /// <summary>
    /// The Repository interface.
    /// </summary>
    /// <typeparam name="TEntity">The entity type the repository works with.
    /// </typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class, IEntity, new()
    {
        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> Get();

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        IEnumerable<TEntity> Get(int id);

        /// <summary>
        /// The get single.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity GetSingle(int id);

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int Add(TEntity entity);

        /// <summary>
        /// The change.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Change(TEntity entity);

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        void Remove(TEntity entity);
    }
}
