﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayPilot.Web.Mvc;
using System.Collections;

namespace ami.Data.UI
{
    public interface ICalendarDataSource
    {
        IEnumerable Load();
        string DataIdField { get;}
        string DataTextField { get;}
        string DataStartField { get;}
        string DataEndField { get;}
    }
}
