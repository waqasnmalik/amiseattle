﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ami.Data.Repositories;
using System.Collections;

namespace ami.Data.UI
{
    public class CalendarDataSource : ICalendarDataSource
    {
        private IRepository<ami.Data.Entities.AmiEvent> eventRepository;

        public CalendarDataSource(IRepository<ami.Data.Entities.AmiEvent> eventRepository)
        {
            this.eventRepository = eventRepository;
        }

        public string DataIdField
        {
            get
            {
                return "Id";
            }
        }

        public string DataTextField
        {
            get
            {
                return "Title";
            }
        }

        public string DataStartField
        {
            get
            {
                return "StartTime";
            }
        }

        public string DataEndField
        {
            get
            {
                return "EndTime";
            }
        }

        public IEnumerable Load()
        {
            var events = this.eventRepository.Get();
            return events;
        }
    }
}