﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ami.Data.Entities
{
    public abstract class Entity : IEntity
    {
        public virtual int Id { get; set; }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}