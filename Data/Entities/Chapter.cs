﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ami.Data.Entities
{
    public sealed class Chapter : Entity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string TwitterWidget { get; set; }
        public string MapLocation { get; set; }
        public bool? IsActive { get; set; }
        public ICollection<EventRegistration> EventRegistrations { get; set; }
    }
}