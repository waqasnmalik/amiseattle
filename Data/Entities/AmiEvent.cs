﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ami.Data.Entities
{
    public sealed class AmiEvent : Entity
    {
        public string Title { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool? IsActive { get; set; }
        public string Image { get; set; }
        public string DetailFile { get; set; }
        public string MtaLink { get; set; }
        public string AlislamLink { get; set; }
        public string YoutubeLink { get; set; }
        public string MapLocation { get; set; }
        public ICollection<Chapter> Chapters { get; set; }
        public ICollection<EventRegistration> EventRegistrations { get; set; }
    }
}