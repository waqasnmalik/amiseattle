﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ami.Data.Entities
{
    public class Contact : Entity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public Chapter Chapter { get; set; }
    }
}