﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ami.Data.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
