﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Migrations;
using ami.Data.Entities;

namespace ami.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AmiDbContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(AmiDbContext context)
        {
            context.AmiEventSet.AddOrUpdate(e => e.Title, CreatePlaceHolderEvents());

            var chapter = context.ChapterSet.First();
            context.ContactSet.AddOrUpdate(e => e.Name, CreateContact(chapter));
        }

        private static Contact CreateContact(Chapter chapter)
        {
            return new Contact { Name = "Placeholder contact", Email = "test@test.org", Message = "Hello!", Phone = "(425)788-0987", Chapter = chapter };
        }

        private static Chapter[] CreateChapters()
        {
            return new Chapter[] 
                    {
                        new Chapter 
                        {
                            Name = "Seattle",
                            Address = "Ahmadiyya Muslim Community 19212 Highway 99<br /> Lynnwood, WA,98036<br />phone:(425)954-7526<br />",
                            ContactNumber = "(425)954-7526",
                            TwitterWidget = "<a class=\"twitter-timeline\" href=\"https://twitter.com/amiseattle\" data-widget-id=\"402179807552606208\">Tweets by amiseattle</a> <script>    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + \"://platform.twitter.com/widgets.js\"; fjs.parentNode.insertBefore(js, fjs); } } (document, \"script\", \"twitter-wjs\");</script>",
                            MapLocation = "<iframe src=\"https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x549005ea37519b35%3A0xac6b78d0e820b490!2s100+172nd+Pl+SE%2C+Bothell%2C+WA+98012!5e0!3m2!1sen!2sus!4v1385517040099\" width=\"300\" height=\"450\" frameborder=\"0\" style=\"border:0\"></iframe>",
                            IsActive = true
                        }
                    };
        }

        private static AmiEvent[] CreatePlaceHolderEvents()
        {
            var chapters = CreateChapters();
            var eventRegistrations = CreateEventRegistrations();

            foreach (var eventRegistration in eventRegistrations)
            {
                eventRegistration.Chapter = chapters.First();
            }

            return new AmiEvent[]
            {
                new AmiEvent
                {
                    Title = "Placeholder Event",
                    Location = "Seattle, WA",
                    Details = "Placeholder event used for testing the web site.",
                    StartTime = new DateTime(2013, 12, 9, 19, 0, 0),
                    EndTime = new DateTime(2013, 12, 9, 21, 0, 0),
                    IsActive = true,
                    Chapters = chapters,
                    EventRegistrations = eventRegistrations
                }
            };
        }

        public static EventRegistration[] CreateEventRegistrations()
        {
            return new EventRegistration[] 
            {
                new EventRegistration
                {
                    Name = "Farooq Mahmud",
                    Email = "farooq@live.org",
                    NumberOfPersons = 3,
                    Chapter = CreateChapters().First(),
                    Message = "Hello!"
                }
            };
        }
    }
}