namespace ami.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AmiEvent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Location = c.String(),
                        Details = c.String(),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        IsActive = c.Boolean(),
                        Image = c.String(maxLength: 255),
                        DetailFile = c.String(maxLength: 255),
                        MtaLink = c.String(maxLength: 255),
                        AlislamLink = c.String(maxLength: 255),
                        YoutubeLink = c.String(maxLength: 255),
                        MapLocation = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Chapter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Address = c.String(maxLength: 500),
                        ContactNumber = c.String(maxLength: 50),
                        TwitterWidget = c.String(maxLength: 500),
                        MapLocation = c.String(maxLength: 500),
                        IsActive = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventRegistration",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(maxLength: 50),
                        Message = c.String(nullable: false),
                        NumberOfPersons = c.Int(),
                        Chapter_Id = c.Int(),
                        AmiEvent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chapter", t => t.Chapter_Id)
                .ForeignKey("dbo.AmiEvent", t => t.AmiEvent_Id)
                .Index(t => t.Chapter_Id)
                .Index(t => t.AmiEvent_Id);
            
            CreateTable(
                "dbo.EventChapter",
                c => new
                    {
                        EventId = c.Int(nullable: false),
                        ChapterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventId, t.ChapterId })
                .ForeignKey("dbo.AmiEvent", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Chapter", t => t.ChapterId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.ChapterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventRegistration", "AmiEvent_Id", "dbo.AmiEvent");
            DropForeignKey("dbo.EventChapter", "ChapterId", "dbo.Chapter");
            DropForeignKey("dbo.EventChapter", "EventId", "dbo.AmiEvent");
            DropForeignKey("dbo.EventRegistration", "Chapter_Id", "dbo.Chapter");
            DropIndex("dbo.EventRegistration", new[] { "AmiEvent_Id" });
            DropIndex("dbo.EventChapter", new[] { "ChapterId" });
            DropIndex("dbo.EventChapter", new[] { "EventId" });
            DropIndex("dbo.EventRegistration", new[] { "Chapter_Id" });
            DropTable("dbo.EventChapter");
            DropTable("dbo.EventRegistration");
            DropTable("dbo.Chapter");
            DropTable("dbo.AmiEvent");
        }
    }
}
