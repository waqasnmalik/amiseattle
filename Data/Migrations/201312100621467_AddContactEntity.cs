namespace ami.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContactEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Message = c.String(),
                        Chapter_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chapter", t => t.Chapter_Id)
                .Index(t => t.Chapter_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contact", "Chapter_Id", "dbo.Chapter");
            DropIndex("dbo.Contact", new[] { "Chapter_Id" });
            DropTable("dbo.Contact");
        }
    }
}
