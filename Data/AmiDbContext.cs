﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ami.Data.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations.Schema;

namespace ami.Data
{
    public class AmiDbContext : DbContext
    {
        private const int ShortStringLength = 50;
        private const int MediumStringLength = 100;
        private const int LongStringLength = 500;
        private const int DefaultStringLength = 255;

        static AmiDbContext()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        public AmiDbContext()
            :base("AmiDb")
        {

        }

        public DbSet<AmiEvent> AmiEventSet { get; set; }
        public DbSet<Chapter> ChapterSet { get; set; }
        public DbSet<EventRegistration> EventRegistrationSet { get; set; }
        public DbSet<Contact> ContactSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Ignore<IEntity>()
                .Ignore<Entity>();

            ConfigureEventTable(modelBuilder);
            ConfigureChapterTable(modelBuilder);
            ConfigureEventRegistrationTable(modelBuilder);
            ConfigureContactTable(modelBuilder);
        }

        private static void ConfigureEventTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AmiEvent>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<AmiEvent>().Property(e => e.Title).IsRequired();
            modelBuilder.Entity<AmiEvent>().Property(e => e.Image).HasMaxLength(DefaultStringLength);
            modelBuilder.Entity<AmiEvent>().Property(e => e.DetailFile).HasMaxLength(DefaultStringLength);
            modelBuilder.Entity<AmiEvent>().Property(e => e.MtaLink).HasMaxLength(DefaultStringLength);
            modelBuilder.Entity<AmiEvent>().Property(e => e.AlislamLink).HasMaxLength(DefaultStringLength);
            modelBuilder.Entity<AmiEvent>().Property(e => e.YoutubeLink).HasMaxLength(DefaultStringLength);
            modelBuilder.Entity<AmiEvent>().Property(e => e.MapLocation).HasMaxLength(DefaultStringLength);

            modelBuilder.Entity<AmiEvent>()
                .HasMany(e => e.Chapters)
                .WithMany()
                .Map(map => map.ToTable("EventChapter").MapLeftKey("EventId").MapRightKey("ChapterId"));

            modelBuilder.Entity<AmiEvent>().HasMany(e => e.EventRegistrations);
        }

        private static void ConfigureChapterTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Chapter>().Property(e => e.Name).IsRequired().HasMaxLength(MediumStringLength);
            modelBuilder.Entity<Chapter>().Property(e => e.Address).HasMaxLength(LongStringLength);
            modelBuilder.Entity<Chapter>().Property(e => e.ContactNumber).HasMaxLength(ShortStringLength);
            modelBuilder.Entity<Chapter>().Property(e => e.TwitterWidget).HasMaxLength(LongStringLength);
            modelBuilder.Entity<Chapter>().Property(e => e.MapLocation).HasMaxLength(LongStringLength);

            modelBuilder.Entity<Chapter>().HasMany(e => e.EventRegistrations);
        }

        private static void ConfigureEventRegistrationTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventRegistration>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<EventRegistration>().Property(e => e.Name).IsRequired().HasMaxLength(MediumStringLength);
            modelBuilder.Entity<EventRegistration>().Property(e => e.Email).IsRequired().HasMaxLength(MediumStringLength);
            modelBuilder.Entity<EventRegistration>().Property(e => e.Phone).HasMaxLength(ShortStringLength);
            modelBuilder.Entity<EventRegistration>().Property(e => e.Message).IsRequired();
        }

        private static void ConfigureContactTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<EventRegistration>().Property(e => e.Name).IsRequired().HasMaxLength(MediumStringLength);
            modelBuilder.Entity<EventRegistration>().Property(e => e.Email).IsRequired().HasMaxLength(MediumStringLength);
            modelBuilder.Entity<EventRegistration>().Property(e => e.Message).IsRequired();
            modelBuilder.Entity<EventRegistration>().Property(e => e.Phone).HasMaxLength(ShortStringLength);
        }
    }
}