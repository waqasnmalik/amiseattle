﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ami.Models;
using ami.Data.Repositories;
using ami.Data.Entities;
using ami.Data.UI;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Events.Month;
using DayPilot.Web.Mvc.Enums;
namespace ami.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<ami.Data.Entities.AmiEvent> eventRepository;
        private IRepository<ami.Data.Entities.Chapter> chapterRepository;
        private IRepository<ami.Data.Entities.Contact> contactRepository;
        private IRepository<ami.Data.Entities.EventRegistration> eventRegistrationRepository;
        private ICalendarDataSource calendarDataSource;
        public HomeController(
            IRepository<AmiEvent> eventRepository,
            IRepository<ami.Data.Entities.Chapter> chapterRepository,
            IRepository<ami.Data.Entities.Contact> contactRepository,
            IRepository<ami.Data.Entities.EventRegistration> eventRegistrationRepository,
            ICalendarDataSource calendarDataSource)
        {
            this.eventRepository = eventRepository;
            this.chapterRepository = chapterRepository;
            this.contactRepository = contactRepository;
            this.eventRegistrationRepository = eventRegistrationRepository;
            this.calendarDataSource = calendarDataSource;
        }
        
        //
        // GET: /Home/

        public ActionResult Index(int id)
        {
            Session["chapterID"] = id;
            var amiModel = new AmiModel(this.chapterRepository);
            amiModel.id = id;
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
            amiModel.id = id;
            return View("Index",amiModel);
        }
        public ActionResult Contact(int chapterID)
        {
            var amiModel = new AmiModel(this.chapterRepository);
            amiModel.id = chapterID;
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
            amiModel.id = chapterID;
            var contect = new ami.Models.Contact();
            contect.chapterid = chapterID;
            contect.saved = false;
            amiModel.contect = contect;
            Session["contectmap"]=amiModel.mapLocation ;
            return View("Contact",amiModel);
        }
        [HttpPost]
        public ActionResult SaveContactUs(AmiModel amiModel)
        {
           // amiModel.mapLocation = Session["contectmap"];
            if (amiModel.contect.PhoneNo==null)
            {
                amiModel.contect.PhoneNo = "";
            }
            else if (!amiModel.contect.IsPhone(amiModel.contect.PhoneNo.ToString()))
            {
                amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
                
                var contect = new ami.Models.Contact();
                
                contect.chapterid = amiModel.id;
                contect.saved = false;
                amiModel.contect = contect;
                amiModel.contect.PhoneNo = "";
                amiModel.id = amiModel.contect.chapterid;

                ModelState.AddModelError("", "Entered phone format is not valid. format is (123-456-7891).");
                return View("Contact", amiModel);
            }
            amiModel.id = amiModel.contect.chapterid;
            amiModel.contect.save(amiModel,this.chapterRepository,this.contactRepository);
           /* SmtpClient client = new SmtpClient("mail.amiseattle.org",465);
            MailAddress from = new MailAddress(amiModel.contect.Email);
            MailAddress to = new MailAddress("sulemanmubarik@gmail.com");
            MailMessage message = new MailMessage(from, to);
            message.Body = amiModel.contect.Message;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = "AMISeattle contect us";
            message.SubjectEncoding = System.Text.Encoding.UTF8;
           // 
            string userState = "contect us";
            client.SendAsync(message, userState);*/
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
            var contect1 = new ami.Models.Contact();
            contect1.saved = true;

            contect1.chapterid = amiModel.id;
            amiModel.contect = contect1;
            return View("Contact",amiModel);
        } 
        public ActionResult About(int chapterID)
        {
            var amiModel = new AmiModel(this.chapterRepository);
            amiModel.id = chapterID;
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
            amiModel.id = chapterID;
            return View(amiModel);
        }
        public ActionResult EventRegistration(int id)
        {
            var @event = this.eventRepository.GetSingle(id);
           
            var amiModel = new AmiModel(this.chapterRepository);
            amiModel.id = id;
           // amiModel.id = chapterID;
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
            var eventRegistration =  new ami.Models.EventRegistrations(this.eventRegistrationRepository, this.eventRepository, this.chapterRepository);
            eventRegistration.saved = false;
            eventRegistration.EventID = id;
            eventRegistration.chapterID = id;
            eventRegistration = eventRegistration.getEvent(eventRegistration);
            amiModel.eventRegistration = eventRegistration;
            return View("EventRegistration", eventRegistration);
        }

        public ActionResult SaveEventRegistration(string Name, string Email, string PhoneNo, string NumberOfPersons, string Message, int EventID)
        {
            var cid = int.Parse(Session["chapterID"].ToString());
            Session["chapterID"] = cid;
            var amiModel = new AmiModel(this.chapterRepository);
            amiModel.id = cid;
            amiModel = amiModel.GetChapter(amiModel, this.chapterRepository);
           
             int i=0;
             bool re = int.TryParse(NumberOfPersons, out i);
           
          
            var chapter = this.chapterRepository.GetSingle(amiModel.id);
            var @event = this.eventRepository.GetSingle(EventID);
            var eventRegistration = new ami.Models.EventRegistrations();
            eventRegistration.EventID = @event.Id;
            eventRegistration.title = @event.Title;
            eventRegistration.Image = @event.Image;
            eventRegistration.location = @event.Location;
            eventRegistration.mapLocation = @event.MapLocation;
            eventRegistration.mtaLink = @event.MtaLink;
            eventRegistration.alislamLink = @event.AlislamLink;
            eventRegistration.details = @event.Details;
            eventRegistration.detailFile = @event.DetailFile;
            eventRegistration.startTime = @event.StartTime.Value.TimeOfDay.ToString();
            eventRegistration.endTime = @event.EndTime.Value.TimeOfDay.ToString();
            eventRegistration.date = @event.StartTime.Value.Date.ToString();
            eventRegistration.youtubeLink = @event.YoutubeLink;
            eventRegistration.mapLocation = Session["eventmap"].ToString();
            eventRegistration.Name=Name;
            eventRegistration.Email=Email;
            eventRegistration.PhoneNo=PhoneNo.ToString();
            eventRegistration.Message=Message;
            eventRegistration.NumberOfPersons=i;
            bool valid = true;
            if (eventRegistration.Name == null || eventRegistration.Name.Equals(""))
            {
                eventRegistration.Name = "";
                ModelState.AddModelError("", "Name is required.");
                valid = false;
            }
            if (eventRegistration.Email == null || eventRegistration.Email.Equals(""))
            {
                ModelState.AddModelError("", "Email is required. format is (ex@abc.com).");
                eventRegistration.Email = "";
                valid = false;
            }
            else if (!eventRegistration.IsValidEmail(eventRegistration.Email))
            {
                eventRegistration.Email = "";
                ModelState.AddModelError("", "Entered email is not valid. format is (ex@abc.com).");
                valid = false;
            }
            if (eventRegistration.PhoneNo==null)
            {
                eventRegistration.PhoneNo = "";
            }
            else if(eventRegistration.PhoneNo.Equals(0))
            {
                eventRegistration.PhoneNo = "";
            }
            else if (!eventRegistration.IsPhone(eventRegistration.PhoneNo.ToString()))
            {
                eventRegistration.PhoneNo = "";
                ModelState.AddModelError("", "Entered phone format is not valid. format is (123-456-7891).");
                valid = false;
            }
           if (!re)
            {
                eventRegistration.NumberOfPersons = 0;
                ModelState.AddModelError("", "Entered number of person is not valid.");
                valid = false;                
            }
            else if(eventRegistration.NumberOfPersons<0)
            {
                eventRegistration.NumberOfPersons = 0;
            }
            
            if (eventRegistration.Message == null || eventRegistration.Message.Equals(""))
            {
                eventRegistration.Message = " ";
            }
            if (!valid)
            {
                return PartialView("EventRegistration", eventRegistration);
            }
            amiModel.eventRegistration = eventRegistration;
            eventRegistration.save(amiModel, chapter, @event, this.eventRegistrationRepository);
            eventRegistration.saved = true;
            eventRegistration.mapLocation = Session["eventmap"].ToString();
            return PartialView("EventRegistration", eventRegistration );
        }

        public ActionResult Calendar()
        {
            return new Calendar(this.calendarDataSource).CallBack(this);
            
        }

        public ActionResult EventDetails(int id)
        {
            var @event = this.eventRepository.GetSingle(id);
            
          
            var eventRegistration = new ami.Models.EventRegistrations();
            eventRegistration.EventID = @event.Id;
            eventRegistration.title = @event.Title;
            eventRegistration.Image = @event.Image;
            eventRegistration.location = @event.Location;
            eventRegistration.mapLocation = @event.MapLocation;
            eventRegistration.mtaLink = @event.MtaLink;
            eventRegistration.alislamLink = @event.AlislamLink;
            eventRegistration.details=@event.Details;
            eventRegistration.detailFile = @event.DetailFile;
            eventRegistration.startTime = @event.StartTime.Value.TimeOfDay.ToString();
            eventRegistration.endTime = @event.EndTime.Value.TimeOfDay.ToString();
            eventRegistration.date = @event.StartTime.Value.Date.ToString();
            eventRegistration.youtubeLink = @event.YoutubeLink;
            Session["eventmap"]  = @event.MapLocation;
            return PartialView("EventRegistration", eventRegistration);
        }
    
    }
}
