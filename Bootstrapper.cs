using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using ami.Data;
using ami.Data.Repositories;
using ami.Data.Entities;
using ami.Data.UI;

namespace ami
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            var context = new AmiDbContext();

            container.RegisterType<IRepository<AmiEvent>, EntityFrameworkRepository<AmiEvent>>(new InjectionConstructor(context));
            container.RegisterType<IRepository<ami.Data.Entities.Chapter>, EntityFrameworkRepository<ami.Data.Entities.Chapter>>(new InjectionConstructor(context));
            container.RegisterType<IRepository<ami.Data.Entities.Contact>, EntityFrameworkRepository<ami.Data.Entities.Contact>>(new InjectionConstructor(context));
            container.RegisterType<IRepository<ami.Data.Entities.EventRegistration>, EntityFrameworkRepository<ami.Data.Entities.EventRegistration>>(new InjectionConstructor(context));
            container.RegisterType<ICalendarDataSource, CalendarDataSource>();
            
            return container;
        }
    }
}