﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using ami.Data.Entities;
using ami.Data.Repositories;
using System.Text.RegularExpressions;

namespace ami.Models
{
    public class AmiModel
    {
        public IRepository<Chapter> chapterRepository;

        public AmiModel()
        {

        }

        public AmiModel(IRepository<Chapter> chapterRepository)
        {
            this.chapterRepository = chapterRepository;
        }
    
        public int id  { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string twitterWidget { get; set; }
        public string mapLocation { get; set; }
        public Contact contect { get; set; }
        public EventRegistrations eventRegistration { get; set; }

        public IEnumerable<AmiChapter> GetChapters(IRepository<ami.Data.Entities.Chapter> chapterRepository)
        {
            var chapters = chapterRepository.Get().Where(c => c.IsActive == true).ToList();

            foreach (var chapter in chapters)
            {
                yield return new AmiChapter { ChapterID = (short)chapter.Id, ChapterName = chapter.Name };
            }
        }

        public AmiModel GetChapter(AmiModel amiModel, IRepository<ami.Data.Entities.Chapter> chapterRepository)
        {
            var chapter = chapterRepository.GetSingle(amiModel.id);
            return new AmiModel(chapterRepository) { id=chapter.Id, name = chapter.Name, address = chapter.Address, twitterWidget = chapter.TwitterWidget, mapLocation = chapter.MapLocation };
        }
    }

    public class AmiChapter
    {
        public short ChapterID { get; set; }
        public string ChapterName { get; set; }
    }

    public class Contact
    {
        private IRepository<Chapter> chapterRepository;
        private IRepository<ami.Data.Entities.Contact> contactRepository;

        public Contact()
        {

        }

        public Contact(
            IRepository<Chapter> chapterRepository, 
            IRepository<ami.Data.Entities.Contact> contactRepository)
        {
            this.chapterRepository = chapterRepository;
            this.contactRepository = contactRepository;
        }

        [Required]
        [DisplayName("Name *")]
        public string Name { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DisplayName("Email * (ex@gmail.com)")]
        public string Email { get; set; }
        [DisplayName("Phone(123-456-7891)")]
       public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Message can not be left blank")]
        [DisplayName("Message *")]    
        public string Message { get; set; }

        public int chapterid { get; set; }
        public bool saved { get; set; }
        public bool IsPhone(String strPhone)
        {
            Regex objPhonePattern = new Regex(@"((\(\d{3}\) ?)|(\d{3}[- \.]))?\d{3}[- \.]\d{4}(\s(x\d+)?){0,1}$");
            return objPhonePattern.IsMatch(strPhone);
        }

        public bool save(AmiModel amiModel, IRepository<ami.Data.Entities.Chapter> chapterRepository, IRepository<ami.Data.Entities.Contact> contactRepository)
        {
            var chapter = chapterRepository.GetSingle(amiModel.id);

            var contact = new ami.Data.Entities.Contact 
            { 
                Name = amiModel.contect.Name, 
                Chapter = chapter, 
                Email = amiModel.contect.Email, 
                Phone = amiModel.contect.PhoneNo.ToString(), 
                Message = amiModel.contect.Message 
            };

            try
            {
                contactRepository.Add(contact);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class EventRegistrations
    {
        private IRepository<ami.Data.Entities.EventRegistration> eventRegistrationRepository;
        private IRepository<AmiEvent> eventRepository;
        private IRepository<ami.Data.Entities.Chapter> chapterRepository;

        public EventRegistrations()
        {

        }

        public EventRegistrations(
            IRepository<ami.Data.Entities.EventRegistration> eventRegistrationRepository,
            IRepository<AmiEvent> eventRepository,
            IRepository<ami.Data.Entities.Chapter> chapterRepository)
        {
            this.eventRegistrationRepository = eventRegistrationRepository;
            this.eventRepository = eventRepository;
            this.chapterRepository = chapterRepository;
        }

        [Required]
        [DisplayName("Name *")]
        public string Name { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [DisplayName("Email * (ex@gmail.com)")]
        public string Email { get; set; }
        [DisplayName("Phone (123-456-7891)")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid. format is (1234567891).")]
        public string PhoneNo { get; set; }
        [DisplayName("Number of persons comming with you")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Only numbers are allowed in this field")]
        public int NumberOfPersons { get; set; }
        [DisplayName("Message")]
        public string Message { get; set; }

        public int EventID { get; set; }
        public int chapterID { get; set; } 
        public string title { get; set; }
        public string location { get; set; }
        public string details { get; set; }
        public string date { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string Image { get; set; }
        public string detailFile { get; set; }
        public string mtaLink { get; set; }
        public string alislamLink { get; set; }
        public string youtubeLink { get; set; }
        public string mapLocation { get; set; }

        public bool saved { get; set; }
        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool IsPhone(String strPhone)
        {
            Regex objPhonePattern = new Regex(@"((\(\d{3}\) ?)|(\d{3}[- \.]))?\d{3}[- \.]\d{4}(\s(x\d+)?){0,1}$");
            return objPhonePattern.IsMatch(strPhone);
        }

        public bool save(AmiModel amiModel, ami.Data.Entities.Chapter chapter, ami.Data.Entities.AmiEvent @event, IRepository<ami.Data.Entities.EventRegistration> eventRegistrationRepository)
        {       

           
            var eventRegistration = new ami.Data.Entities.EventRegistration
            {
                Name = amiModel.eventRegistration.Name,
                Chapter = chapter,
                Email = amiModel.eventRegistration.Email,
                Phone = amiModel.eventRegistration.PhoneNo.ToString(),
                Message = amiModel.eventRegistration.Message,
                AmiEvent = @event,
                NumberOfPersons = amiModel.eventRegistration.NumberOfPersons
            };

            try
            {
                eventRegistrationRepository.Add(eventRegistration);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public EventRegistrations getEvent(EventRegistrations eventRegistration)
        {
            var @event = this.eventRepository.GetSingle(eventRegistration.EventID);

            return new EventRegistrations
            {
                location = @event.Location,
                details = @event.Details,
                date = @event.StartTime.ToString(),
                startTime = @event.StartTime.ToString(),
                endTime = @event.EndTime.ToString(),
                Image = @event.Image,
                detailFile = @event.DetailFile,
                mtaLink = @event.MtaLink,
                alislamLink = @event.AlislamLink,
                youtubeLink = @event.YoutubeLink,
                mapLocation = @event.MapLocation
            };
        }
    }
}