﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DayPilot.Web.Mvc;
using ami.Data.UI;
using DayPilot.Web.Mvc.Events.Month;
using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Events.Month;
using DayPilot.Web.Mvc.Enums;

namespace ami.Models
{
    public class Calendar : DayPilotMonth
    {
        private ICalendarDataSource calendarDataSource;

        public Calendar(ICalendarDataSource calendarDataSource)
            : base()
        {
            this.calendarDataSource = calendarDataSource;
            this.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }

        protected override void OnInit(DayPilot.Web.Mvc.Events.Month.InitArgs e)
        {
            this.Events = this.calendarDataSource.Load();
            this.DataIdField = this.calendarDataSource.DataIdField;
            this.DataTextField = this.calendarDataSource.DataTextField;
            this.DataStartField = this.calendarDataSource.DataStartField;
            this.DataEndField = this.calendarDataSource.DataEndField;
            this.Update();
        }
    }
}